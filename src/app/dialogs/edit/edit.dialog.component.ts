import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../data.service';
import {NumberValidators} from '../numberValidator';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-baza.dialog',
  templateUrl: '../../dialogs/edit/edit.dialog.html',
  styleUrls: ['../../dialogs/edit/edit.dialog.css']
})
export class EditDialogComponent {

  editForm: FormGroup = new FormGroup({});

  constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dataService: DataService,
              private fb: FormBuilder) {
    this.editForm = fb.group({
      'id': ['', [Validators.required, NumberValidators.isNumberCheck()]],
      'name': ['', [Validators.required]],
      'age': ['', [Validators.required, NumberValidators.isNumberCheck()]],
      'salary': ['', [Validators.required, NumberValidators.isNumberCheck()]]
    });
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.dataService.updateUser(this.data);
  }
}
