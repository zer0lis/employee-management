import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../data.service';
import {User} from '../../user.model';
import {NumberValidators} from '../numberValidator';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent {

  addForm: FormGroup = new FormGroup({});

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: User,
              public dataService: DataService,
              private fb: FormBuilder) {

    this.addForm = fb.group({
      'id': ['', [Validators.required, NumberValidators.isNumberCheck()]],
      'name': ['', [Validators.required]],
      'age': ['', [NumberValidators.isNumberCheck()]],
      'salary': ['', [NumberValidators.isNumberCheck()]]
    });
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addUser(this.data);
  }
}
