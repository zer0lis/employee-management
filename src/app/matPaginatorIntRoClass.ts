import {MatPaginatorIntl} from '@angular/material';

export class MatPaginatorIntRo extends MatPaginatorIntl {
  itemsPerPageLabel = 'Nr de randuri pe pagina';
  nextPageLabel     = 'Pagina Urmatoare';
  previousPageLabel = 'Pagina Precedenta';
}
