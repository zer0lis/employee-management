import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {User} from './user.model';

@Injectable()
export class DataService {

  dataChange: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  dialogData: any;

  constructor () {}

  get data(): User[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllUsers(): any {
    return this.dataChange.next(this.randomUsers());
  }
  addUser (user: User): void {
    this.dialogData = user;
  }
  updateUser (user: User): void {
    this.dialogData = user;
  }
  deleteUser (id: number): void {
    console.log(id);
  }
  private randomUsers(): User[] {
    const names: Array<string> = ['Andrei', 'Paul', 'Alexandru', 'Valise', 'Petruta', 'Ioana', 'Camelia', 'Oana', 'Razvan'];
    const sirNames: Array<string> = ['Botez', 'Mihaila', 'Popescu', 'Ionescu', 'Munteanu', 'Olteanu', 'Negru', 'Contra', 'Pusca'];
    let numberRandomRecords = 12; // how many random rows
    let randomNames: User[] = [];
    while (numberRandomRecords > 0) {
      randomNames.push({
        id: numberRandomRecords,
        name: names[this.getRandomInt(0, names.length)] + ' ' +  sirNames[this.getRandomInt(0, sirNames.length)],
        age: this.getRandomInt(18, 70),
        salary: this.getRandomInt(500, 10000)
      });
      numberRandomRecords--;
    }
    return randomNames;
  }
  private getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }
}


/* API Methods for a backend. Could use ToasterService and Material Toasts for displaying messages:
    // ADD, POST METHOD
    addItem(kanbanItem: KanbanItem): void {
    this.httpClient.post(this.API_URL, kanbanItem).subscribe(data => {
      this.dialogData = kanbanItem;
      this.toasterService.showToaster('Successfully added', 3000);
      },
      (err: HttpErrorResponse) => {
      this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
    });
   }
    // UPDATE, PUT METHOD
     updateItem(kanbanItem: KanbanItem): void {
    this.httpClient.put(this.API_URL + kanbanItem.id, kanbanItem).subscribe(data => {
        this.dialogData = kanbanItem;
        this.toasterService.showToaster('Successfully edited', 3000);
      },
      (err: HttpErrorResponse) => {
        this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }
  // DELETE METHOD
  deleteItem(id: number): void {
    this.httpClient.delete(this.API_URL + id).subscribe(data => {
      console.log(data['']);
        this.toasterService.showToaster('Successfully deleted', 3000);
      },
      (err: HttpErrorResponse) => {
        this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }
*/
